Tested on Windows 10

### Create Python virtual environment if you want
```
python -m venv venv
venv\Scripts\activate.bat
```


---
### Installing Gstreamer on Windows 10

1- Go to https://gstreamer.freedesktop.org/data/pkg/windows/ and choose the latest version packages. Download both the developer (devel) package and the default package.
Example: gstreamer-1.0-mingw-x86-1.16.2.msi
         gstreamer-1.0-devel-mingw-x86-1.16.2.msi
         (08-03-2020)
         
2- Install the downloaded gstreamer packages, during installation, choose the **COMPLETE** package.

3- Run these command prompt lines:
    *  cd\
    *  cd C :\gstreamer\1.0\x86_64\bin
4- Search for the **gst-launch-1.0.exe** file and go to that directory in the command prompt. 

5- Run a test:
    * gst-launch-1.0.exe videotestsrc ! autovideosink
---
### Gstreamer commands

1- Record a video: 
    * gst-launch-1.0 autovideosrc ! autovideosink
2- Record a video with recording: 
    * gst-launch-1.0 autovideosrc ! autovideosink autoaudiosrc ! autoaudiosink
